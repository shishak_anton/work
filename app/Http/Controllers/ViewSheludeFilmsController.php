<?php

namespace App\Http\Controllers;

use App\Parser;
use Illuminate\Http\Request;
use App\ViewSheludeFilms;

class ViewSheludeFilmsController extends Controller
{
    static function ViewShelude()
    {
        $sheludes = ViewSheludeFilms::GetShelude();
        return view('get', compact('sheludes'));
    }

}
