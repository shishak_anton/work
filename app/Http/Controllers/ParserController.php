<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Symfony\Component\DomCrawler\Crawler;
use App\Parser;


class ParserController extends Controller
{
    public function Parser()
    {
        $link = 'https://cityopen.ru/afisha/kinoteatr-salavat/';
        $cinema = 'h1';
        $hall = 'table tbody tr';
        $film = 'body table tbody';
        $date_view = 'table tbody td';

        $html = file_get_contents($link);
        $crawler = new Crawler(null, $link);
        $crawler->addHtmlContent($html, 'UTF-8');
        $cinema = $crawler->filter($cinema)->text();
        $date_view = $crawler->filter($date_view)->eq(1)->text();
        $hall = $crawler->filter($hall)->eq(1)->text();

        // Собираем данные зеленого зала
        $film_count = $crawler->filter($film)->eq(2)->children()->count();
        $i=1;
        while ($i <= $film_count-1) {

            $film = 'body table tbody';
            $time_view = 'body table tbody';
            $price = 'body table tbody';

            $html = file_get_contents($link);
            $crawler = new Crawler(null, $link);
            $crawler->addHtmlContent($html, 'UTF-8');
            $film = $crawler->filter($film)->eq(2)->children()->eq($i)->children()->eq(1)->each(function (Crawler $node, $i) {
                return $node->text();
            });
            $time_view = $crawler->filter($time_view)->eq(2)->children()->eq($i)->children()->eq(0)->each(function (Crawler $node, $i) {
                return $node->text();
            });
            $price = $crawler->filter($price)->eq(2)->children()->eq($i)->children()->eq(3)->each(function (Crawler $node, $i) {
                return $node->text();
            });

            if ( !empty($film)) {
                foreach ( $film as $filmdata ) {
                    $film = $filmdata ;
                }
            }

            if ( !empty($time_view)) {
                foreach ( $time_view as $timedata ) {
                    $time_view = $timedata ;
                }
            }

            if ( !empty($price)) {
                foreach ( $price as $pricedata) {
                    $price = $pricedata ;
                }
            }

            Parser::ParserDataSave($cinema, $hall, $film, $date_view, $time_view, $price);

            /*
            $content = [
                'link' => $link,
                'cinema' => $cinema,
                'hall' => $hall,
                'date_view' => $date_view,
                'film' => $film,
                'price' => $price,
                'time_view' => $time_view
            ];
            */
            $i++;
        }

        //Выгруожаем данные красного зала
        $hall = 'table tbody';
        $film = 'body table tbody';
        $hall = $crawler->filter($hall)->eq(3)->text();
        $film_count = $crawler->filter($film)->eq(4)->children()->count();

        $i=1;
        while ($i <= $film_count-1) {

            $film = 'body table tbody';
            $time_view = 'body table tbody';
            $price = 'body table tbody';

            $html = file_get_contents($link);
            $crawler = new Crawler(null, $link);
            $crawler->addHtmlContent($html, 'UTF-8');
            $film = $crawler->filter($film)->eq(4)->children()->eq($i)->children()->eq(1)->each(function (Crawler $node, $i) {
                return $node->text();
            });
            $time_view = $crawler->filter($time_view)->eq(4)->children()->eq($i)->children()->eq(0)->each(function (Crawler $node, $i) {
                return $node->text();
            });
            $price = $crawler->filter($price)->eq(4)->children()->eq($i)->children()->eq(3)->each(function (Crawler $node, $i) {
                return $node->text();
            });

            if ( !empty($film)) {
                foreach ( $film as $filmdata ) {
                    $film = $filmdata ;
                }
            }

            if ( !empty($time_view)) {
                foreach ( $time_view as $timedata ) {
                    $time_view = $timedata ;
                }
            }

            if ( !empty($price)) {
                foreach ( $price as $pricedata) {
                    $price = $pricedata ;
                }
            }

            Parser::ParserDataSave($cinema, $hall, $film, $date_view, $time_view, $price);
            /*
            $content = [
                'link' => $link,
                'cinema' => $cinema,
                'hall' => $hall,
                'date_view' => $date_view,
                'film' => $film,
                'price' => $price,
                'time_view' => $time_view
            ];
            */
            $i++;
        }
        $content = 'Данные собраны, переходите к просмотру';
        return view('update', compact('content'));
    }
}
