<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Parser extends Model
{
    public static function ParserDataSave($cinema, $hall, $film, $date_view, $time_view, $price)
    {
        DB::insert('insert into view_shelude_films (cinema, hall, film, date_view, time_view, price) values (?,?,?,?,?,?)',
            [$cinema, $hall, $film, $date_view, $time_view, $price]);
    }
}
