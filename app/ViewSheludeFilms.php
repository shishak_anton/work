<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ViewSheludeFilms extends Model
{
    static function GetShelude()
    {
        $sheludes = DB::table('view_shelude_films')->get();
        return $sheludes;
    }
}
