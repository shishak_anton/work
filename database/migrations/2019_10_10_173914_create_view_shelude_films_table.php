<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewSheludeFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('view_shelude_films', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cinema');
            $table->string('hall');
            $table->string('film');
            $table->string('date_view');
            $table->time('time_view');
            $table->bigInteger('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_shelude_films');
    }
}
