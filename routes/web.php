<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
Route::get('resultparser', function () {
    $sheludes = DB::table('view_shelude_films')->get();
    return view('resultparser', compact('sheludes'));
});
*/

/*Route::get('viewparser', function () {
    return view('parser');
});*/

Route::get('get', 'ViewSheludeFilmsController@ViewShelude');
Route::get('update', 'ParserController@Parser');

